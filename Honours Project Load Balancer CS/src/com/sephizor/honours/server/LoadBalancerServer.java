package com.sephizor.honours.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import com.sephizor.honours.load.Algorithm;
import com.sephizor.honours.load.FastestFirst;
import com.sephizor.honours.load.LeastClients;
import com.sephizor.honours.load.LeastLoad;
import com.sephizor.honours.load.RoundRobin;
import com.sephizor.utils.file.SLogger;
import com.sephizor.utils.net.SSocketHelper;

public class LoadBalancerServer
{
    private ServerSocket clientListen;
    private ServerSocket serverListen;
    private ServerSocket controlProtocol;
    private volatile ArrayList<Socket> availableServers;
    private volatile HashMap<Socket, String> serverStats;
    private SLogger logger;
    private Algorithm alg;

    public LoadBalancerServer(int port)
    {
        logger = SLogger.getInstance(".", "load-balancer.log");
        try
        {
            clientListen = new ServerSocket(port);
            serverListen = new ServerSocket(12001);
            controlProtocol = new ServerSocket(12002);
            availableServers = new ArrayList<Socket>();
            serverStats = new HashMap<Socket, String>();
            alg = new FastestFirst();
        }
        catch(IOException e)
        {
            logger.errorMessage("Couldn't start server on port " + port);
            System.exit(-1);
        }

        Thread t1 = new Thread(new Runnable() {
            public void run()
            {
                listenForServers();
            }
        });
        t1.setName("Server Listener");
        t1.start();
        Thread t2 = new Thread(new Runnable() {
            public void run()
            {
                listenForControl();
            }
        });
        t2.setName("Control Protocol Listener");
        t2.start();
        listenForClients();
    }

    public void listenForServers()
    {
        while(true)
        {
            try
            {
                //Get connection from the connecting server
                final Socket server = serverListen.accept();
                logger.infoMessage("Server connection from: " + server.getInetAddress().toString().substring(1, server.getInetAddress().toString().length()));
                System.out.println("Server connection from: " + server.getInetAddress().toString().substring(1, server.getInetAddress().toString().length()));
                //Add the server to the list of connected servers
                availableServers.add(server);
                
                //Monitoring thread to balance load on the server if it becomes too high
                new Thread(new Runnable() {
                    public void run() {
                        while(true) {
                            try {
                                getServerStats(server);
                                //Split the stats on a ':' delimiter into separate Strings
                                String[] stats = serverStats.get(server).split(":");
                                int connectedClients = Integer.parseInt(stats[0]);
                                int maxEvents = Integer.parseInt(stats[1]);
                                int currentEvents = Integer.parseInt(stats[2]);
                                if(currentEvents >= maxEvents) {
                                    System.out.println();
                                    //Get a new server to send load to
                                    Socket toServer = selectServer();
                                    //Send only half of the number of connected clients
                                    int numClientsToSend = (int) (connectedClients / 2);
                                    //Tell the server an IP address to send load to and the number of clients to send
                                    SSocketHelper.writeToSocket(server, "SEND:clients:" + toServer.getInetAddress().toString().substring(1, server.getInetAddress().toString().length()) + ":" + numClientsToSend + "\n");
                                }
                                
                                //Print server stats to the console
                                System.out.println(server.getInetAddress().toString().substring(1, server.getInetAddress().toString().length()) + ": " + serverStats.get(server));
                            }
                            catch(Exception e) {
                                //If the server becomes unavailable, remove it from the list
                                availableServers.remove(server);
                            }
                            try {
                                //Run every 10 seconds
                                Thread.sleep(10000);
                            }
                            catch(InterruptedException e) {}
                        }
                    }
                }).start();

            }
            catch(IOException e)
            {
                logger.errorMessage(e);
            }
        }
    }
    
    public void listenForControl()
    {
        while(true)
        {
            try
            {
                final Socket controller = controlProtocol.accept();
                logger.infoMessage("Control connection from: " + controller.getInetAddress().toString().substring(1, controller.getInetAddress().toString().length()));
                System.out.println("Control connection from: " + controller.getInetAddress().toString().substring(1, controller.getInetAddress().toString().length()));
                new Thread(new Runnable() {
                    public void run() {
                        while(true) {
                            try
                            {
                                BufferedReader sockIn = new BufferedReader(new InputStreamReader(controller.getInputStream()));
                                String line = "";
                                while((line = sockIn.readLine()) != null) {
                                    switch(line) {
                                        case "stopserver":
                                            System.exit(0);
                                            break;
                                        case "refreshServerStats":
                                            serverStats.clear();
                                            for(Socket s : availableServers) {
                                                try {
                                                    getServerStats(s);
                                                    System.out.println(serverStats.get(s));
                                                }
                                                catch(Exception e) {
                                                    availableServers.remove(s);
                                                }
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                            catch(IOException e) {}
                        }
                    }
                }).start();
            }
            catch(IOException e)
            {
                logger.errorMessage(e);
            }
        }
    }

    public void listenForClients()
    {
        while(true)
        {
            try
            {
              //Get a socket for the connecting client (blocking call)
                final Socket currentClient = clientListen.accept();
                logger.infoMessage("Connection from: " + currentClient.getInetAddress().toString().substring(1, currentClient.getInetAddress().toString().length()));
                System.out.println("Connection from: " + currentClient.getInetAddress().toString().substring(1, currentClient.getInetAddress().toString().length()));
                new Thread(new Runnable() {
                    public void run() {
                        try
                        {
                            //Use the algorithm to select a server
                            Socket selectedServer = selectServer();
                            //Write the IP address of the selected server to the client socket
                            SSocketHelper.writeToSocket(currentClient, selectedServer.getInetAddress().toString().substring(1, selectedServer.getInetAddress().toString().length()) + "\n");
                        }
                        catch(IOException e)
                        {
                            logger.errorMessage(e);
                        }
                    }
                }).start();
            }
            catch(IOException e)
            {
                logger.errorMessage(e);
            }
        }
    }

    private synchronized void getServerStats(Socket s)
    {
        try {
            SSocketHelper.writeToSocket(s, "GET:ReportStats\n");
            //Check if server already exists in stats table
            String testMap = serverStats.get(s);
            if(testMap != null)
                serverStats.remove(s); //If it does, reset it
            //Add the new stats to the map
            serverStats.put(s,  SSocketHelper.readFromSocket(s, true));
        }
        catch(IOException e) {
            availableServers.remove(s);
        }
    }
    
    private Socket selectServer()
    {
        long beforeTime = System.nanoTime(); //Get the current time
        Socket selectedSocket = alg.select(serverStats, availableServers);
        long afterTime = System.nanoTime(); //Time after algorithm is finished
        System.out.println("Took " + (afterTime - beforeTime) + " nanoseconds to select server");
        return selectedSocket;
    }

    public static void main(String[] args)
    {
        if(args.length > 0)
        {
            int port;
            try
            {
                port = Integer.parseInt(args[0]);
            }
            catch(NumberFormatException e)
            {
                port = 12000;
            }
            new LoadBalancerServer(port);
        }
        else {
            new LoadBalancerServer(12000);
        }
    }
}
