package com.sephizor.honours.load;

import java.net.Socket;
import java.util.ArrayList;

public class RoundRobin extends Algorithm
{
    
    private int index;
    
    public RoundRobin() {
        index = 0;
    }

    @Override
    public Socket select(ArrayList<Socket> servers)
    {
        Socket serverToReturn = servers.get(index);
        if(index < servers.size()-1)
            index++;
        else
            index = 0;
        return serverToReturn;
    }

}
