package com.sephizor.honours;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import com.sephizor.honours.event.Event;
import com.sephizor.honours.event.SpecialEvent;
import com.sephizor.utils.net.SSocketHelper;

public class GameClientP2P
{

    private Socket peer;
    private Socket loadBalancer;
    private ArrayList<Socket> peers;
    private ServerSocket peerListen;
    private volatile Socket server;
    private volatile boolean canSendEvents;
    private volatile boolean canSendSpecialEvents;
    private final int eventsPerMin = 20;
    private Timer eventTimer;
    private TimerTask eventTask;

    public GameClientP2P(String authIP, int authPort)
    {
        
    	peers = new ArrayList<Socket>();
        try
        {
            peerListen = new ServerSocket(12020);
        }
        catch(IOException e)
        {
            System.err.println("Cannot listen on port 12020");
        }
        canSendEvents = true;
        canSendSpecialEvents = true;
        connect(authIP, authPort);
        if(peer == null) {
            /* Happens if we're the first client. Solution is the provider runs a single client to be the first on the network,
             * will do nothing other than process events
            */
            canSendEvents = false;
            canSendSpecialEvents = false;
        }
        
        new Thread(new Runnable() {
            public void run() {
                listenForPeers();
            }
        }, "Peer Listener").start();
        
        new Thread(new Runnable() {
            public void run() {
                listenForRequests();
            }
        }, "Request Listener").start();
        
        eventTimer = new Timer("Event Timer");
        eventTask = new TimerTask() {
            public void run()
            {
                Random rand = new Random();
                int eventType = rand.nextInt(4) + 1;
                if(eventType != 4)
                {
                    if(canSendEvents)
                    {
                        try
                        {
                            sendEvent(eventType);
                        }
                        catch(IOException e)
                        {
                        }
                    }
                }
                else if(eventType == 4)
                {
                    if(canSendSpecialEvents)
                    {
                        try
                        {
                            sendEvent(eventType);
                        }
                        catch(IOException e)
                        {
                        }
                    }
                }
            }
        };
        eventTimer.schedule(eventTask, 0, (int) (60000 / eventsPerMin));
        
        new Thread(new Runnable() {
            public void run()
            {
                try
                {
                    while(true)
                    {
                        String line = SSocketHelper.readFromSocket(server, true);
                        String[] command = line.split(":");
                        if(command[0].equals("ChangeServer"))
                        {
                            System.out.println("Changing server to " + command[1]);
                            canSendSpecialEvents = false;
                            server.close();
                            server = new Socket(command[1], 12010);
                            canSendSpecialEvents = true;
                        }
                    }
                }
                catch(IOException e)
                {
                	System.exit(-1);
                }
            }
        }, "Control Protocol").start();
        
    }

    private void listenForPeers()
    {
    	while(true) {
	        try
	        {
	            final Socket client = peerListen.accept();
	            peers.add(client);
	            new Thread(new Runnable() {
	                public void run()
	                {
	                    while(true)
	                    {
	                        try
	                        {
	                            ObjectInputStream sockIn = new ObjectInputStream(client.getInputStream());
	                            Event event = (Event) sockIn.readObject();
	                            processEvent(event);
	                        }
	                        catch(IOException | ClassNotFoundException e)
	                        {
	                            peers.remove(client);
	                            break;
	                        }
	                    }
	                }
	
	                private void processEvent(Event event)
	                {
	                    event.printPayload();
	                }
	            }).start();
	        }
	        catch(IOException e)
	        {
	            e.printStackTrace();
	        }
    	}
    }
    
    public void listenForRequests()
    {
    	System.out.println("Listening for requests");
    	while(true) {
	        try {
	            String line = SSocketHelper.readFromSocket(loadBalancer, true);
	            String[] request = line.split(":");
	            if(request.length == 1)
	            	continue;
	            String method = request[0];
	            String function = request[1];
	    
	            if(method.equals("CHNG"))
	            {
	                switch(function)
	                {
	                    case "Peer":
	                        System.out.println("Changing peer to " + request[2]);
	                        canSendEvents = false;
	                        peer.close();
	                        peer = new Socket(request[2], 12020);
	                        canSendEvents = true;
	                        break;
	                    default:
	                        break;
	                }
	            }
	        }
	        catch(IOException e) {
	            System.exit(-1);
	        }
    	}
    }

    private void connect(String ip, int port)
    {
        try
        {
            System.out.println("Connecting to Load Balancer");
            loadBalancer = new Socket(ip, port);
            String line = SSocketHelper.readFromSocket(loadBalancer, true);
            String[] serverInfo = line.split(":");
            String serverIP = serverInfo[0];
            System.out.println("Connecting to server: " + serverIP);
            server = new Socket(serverIP, 12010);
            try {
                serverIP = serverInfo[1];
                System.out.println("Connecting to next client: " + serverIP);
                peer = new Socket(serverIP, 12020);
            }
            catch(IndexOutOfBoundsException ex) {
                peer = null;
            }
        }
        catch(UnknownHostException e)
        {
            e.printStackTrace();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    private void sendEvent(int eventType) throws IOException
    {
        Event ev = null;
        if(eventType == 4)
        {
            ev = new SpecialEvent();
        }
        else
        {
            ev = new Event(eventType);
        }
        ObjectOutputStream sockOut = null;
        if(ev instanceof SpecialEvent)
        	sockOut = new ObjectOutputStream(server.getOutputStream());
        else
        	sockOut = new ObjectOutputStream(peer.getOutputStream());
        sockOut.writeObject(ev);
    }

    public static void main(String[] args)
    {
        new GameClientP2P(args[0], Integer.parseInt(args[1]));
    }

}
