package com.sephizor.honours.event;

public class SpecialEvent extends Event
{

    private static final long serialVersionUID = -6252502836486782804L;

    public SpecialEvent()
    {
        payload = "Special Event";
    }

}
