package com.sephizor.honours.network;

import java.io.Serializable;
import java.net.Socket;

public class SerializedSocket extends Socket implements Serializable {
    private static final long serialVersionUID = 2206656900902534731L;
}
