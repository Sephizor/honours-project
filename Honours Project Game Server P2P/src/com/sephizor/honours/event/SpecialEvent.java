package com.sephizor.honours.event;

import java.io.Serializable;

public class SpecialEvent extends Event implements Serializable
{

    private static final long serialVersionUID = -6252502836486782804L;
    
    public SpecialEvent()
    {
        payload = "Special Event";
    }

}
