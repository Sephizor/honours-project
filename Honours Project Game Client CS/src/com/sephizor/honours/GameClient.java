package com.sephizor.honours;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;

import com.sephizor.honours.event.Event;
import com.sephizor.utils.net.SSocketHelper;

public class GameClient
{
    
    private Socket initialConnection;
    private volatile Socket passedConnection;
    private volatile boolean canSendEvents;
    private final int eventsPerMin = 20;
    
    public GameClient(String authIP, int authPort) {
        canSendEvents = true;
        connect(authIP, authPort);
        new Thread(new Runnable() {
            public void run() {
                while(true) {
                    try {
                        Random rand = new Random();
                        //Generate the Event ID
                        int eventType = rand.nextInt(3)+1;
                        //If able to send events
                        if(canSendEvents) {
                            try {
                                sendEvent(eventType);
                            }
                            catch(IOException e)
                            {
                                System.exit(-1);
                            }
                        }
                        //Send only the allowed number of events per minute
                        Thread.sleep((int)(60000/eventsPerMin));
                    }
                    catch(InterruptedException e) {
                    }
                }
            }
        }, "Event Thread").start();
        new Thread(new Runnable() {
            public void run() {
                try
                {
                    while(true) {
                        //Get the control message
                        String line = SSocketHelper.readFromSocket(passedConnection, true);
                        String[] command = line.split(":");
                        if(command[0].equals("ChangeServer")) {
                        	System.out.println("Changing server to " + command[1]);
                        	  //Set flag so that newly generated events are not sent
                            canSendEvents = false;
                            //Close the connection to the server
                            passedConnection.close();
                            //Connect to the new server
                            passedConnection = new Socket(command[1], 12010);
                            //Enable event sending again
                            canSendEvents = true;
                        }
                    }
                }
                catch(IOException e)
                {
                	System.exit(-1);
                }
            }
        }, "Control Thread").start();
    }
    
    private void connect(String ip, int port) {
        try
        {
            System.out.println("Connecting to Load Balancer");
            //Connect to the load balancer
            initialConnection = new Socket(ip, port);
            //Get the Server IP
            String line = SSocketHelper.readFromSocket(initialConnection, true);
            String[] serverInfo = line.split(":");
            String serverIP = serverInfo[0];
            System.out.println("Connecting to server: " + serverIP);
            //Connect to the server
            passedConnection = new Socket(serverIP, 12010);
            //Disconnect from the load balancer
            initialConnection.close();
        }
        catch(UnknownHostException e)
        {
        	e.printStackTrace();
            System.exit(-1);
        }
        catch(IOException e)
        {
        	e.printStackTrace();
            System.exit(-1);
        }
    }
    
    private synchronized void sendEvent(int eventType) throws IOException {
        Event ev = new Event(eventType);
        ObjectOutputStream sockOut = new ObjectOutputStream(passedConnection.getOutputStream());
        sockOut.writeObject(ev);
    }
    
    public static void main(String[] args) {
        new GameClient(args[0], Integer.parseInt(args[1]));
    }
    
}
