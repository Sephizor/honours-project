package com.sephizor.honours.event;

import java.io.Serializable;
import java.net.Socket;

public class Event implements Serializable
{
    private static final long serialVersionUID = -1345469818849611838L;
    public static final int NPC_EVENT = 1;
    public static final int CLIENT_EVENT = 2;
    public static final int ZONE_EVENT = 3;
    
    private String payload;
    
    public Event(int type) {
        switch(type) {
            case NPC_EVENT:
                payload = "NPC Event";
                break;
            case CLIENT_EVENT:
                payload = "Client Event";
                break;
            case ZONE_EVENT:
                payload = "Zone Event";
                break;
            default:
                payload = "";
                break;
        }
    }
    
    public void printPayload(Socket client) {
        System.out.println(client.getInetAddress().toString().substring(1, client.getInetAddress().toString().length()) + ": " + payload);
    }
    
}
