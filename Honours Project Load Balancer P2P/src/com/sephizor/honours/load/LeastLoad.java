package com.sephizor.honours.load;

import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

public class LeastLoad extends Algorithm
{
    
    public Socket select(HashMap<Socket, String> serverStats, ArrayList<Socket> availableServers)
    {
    	int leastLoad = Integer.parseInt(serverStats.get(availableServers.get(0)).split(":")[2]);
        Socket least = availableServers.get(0);
        for(Socket s : availableServers) {
            String[] stats = serverStats.get(s).split(":");
            int currentEvents = Integer.parseInt(stats[2]);
            if(currentEvents < leastLoad || currentEvents == 0) {
                leastLoad = currentEvents;
                least = s;
            }
        }
        
        return least;
    }

}
