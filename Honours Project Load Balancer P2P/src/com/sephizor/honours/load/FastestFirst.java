package com.sephizor.honours.load;

import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

public class FastestFirst extends Algorithm
{
    public Socket select(HashMap<Socket, String> serverStats, ArrayList<Socket> availableServers)
    {
    	int fastestFound = 0;
        Socket fastest = availableServers.get(0);
        for(Socket s : availableServers) {
            String[] stats = serverStats.get(s).split(":");
            int maxEvents = Integer.parseInt(stats[1]);
            int currentClients = Integer.parseInt(stats[0]);
            int currentEvents = Integer.parseInt(stats[2]);
            int speed = 0;
            if(currentClients == 0 || currentEvents == 0) {
            	fastest = s;
            	break;
            }
            else {
            	speed = maxEvents / ((int) Math.floor(currentEvents/currentClients));
            }
            if(speed > fastestFound) {
                fastestFound = speed;
                fastest = s;
            }
        }
        
        return fastest;
    }
}
