package com.sephizor.honours.load;

import java.net.Socket;
import java.util.ArrayList;

public class RoundRobin extends Algorithm
{
    
    private volatile int serverIndex;
    private volatile int clientIndex;
    
    public RoundRobin() {
        serverIndex = 0;
        clientIndex = 0;
    }

    @Override
    public Socket select(ArrayList<Socket> servers, boolean isClient)
    {
    	Socket serverToReturn;
        if(!isClient) {
        	serverToReturn = servers.get(serverIndex);
        	serverIndex++;
        	if(serverIndex == servers.size())
        		serverIndex = 0;
        }
        else {
        	serverToReturn = servers.get(clientIndex);
        	clientIndex++;
        	if(clientIndex > servers.size())
        		clientIndex = 0;
        }
        return serverToReturn;
    }
    
    public void removeClient() {
    	clientIndex--;
    }

}
