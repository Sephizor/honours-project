package com.sephizor.honours.load;

import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

public class LeastClients
{
    
    public Socket select(HashMap<Socket, String> serverStats, ArrayList<Socket> availableServers)
    {
    	int leastClients = Integer.parseInt(serverStats.get(availableServers.get(0)).split(":")[0]);
        Socket least = availableServers.get(0);
        for(Socket s : availableServers) {
            String[] stats = serverStats.get(s).split(":");
            int currentClients = Integer.parseInt(stats[0]);
            if(currentClients < leastClients || currentClients == 0) {
                leastClients = currentClients;
                least = s;
            }
        }
        
        return least;
    }
    
}
