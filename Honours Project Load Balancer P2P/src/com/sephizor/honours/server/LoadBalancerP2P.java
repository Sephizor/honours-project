package com.sephizor.honours.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import com.sephizor.honours.load.Algorithm;
import com.sephizor.honours.load.RoundRobin;
import com.sephizor.utils.net.SSocketHelper;

public class LoadBalancerP2P
{
    
    private ServerSocket clientListen;
    private ServerSocket serverListen;
    private volatile ArrayList<Socket> availableHandlers;
    private volatile ArrayList<Socket> availableSpecialHandlers;
    private volatile HashMap<Socket, String> serverStats;
    private volatile HashMap<Socket, ArrayList<Socket>> peerPairs;
    private Algorithm alg;
    private RoundRobin alg2;

    public LoadBalancerP2P(int port)
    {
        
        try
        {
            clientListen = new ServerSocket(port);
            serverListen = new ServerSocket(12001);
            availableSpecialHandlers = new ArrayList<Socket>();
            serverStats = new HashMap<Socket, String>();
            peerPairs = new HashMap<Socket, ArrayList<Socket>>();
            availableHandlers = new ArrayList<Socket>();
            
            alg = new RoundRobin();
            alg2 = new RoundRobin();
        }
        catch(IOException e)
        {
            System.err.println("Couldn't start server on port " + port);
            System.exit(-1);
        }

        Thread t1 = new Thread(new Runnable() {
            public void run()
            {
                listenForServers();
            }
        });
        t1.setName("Server Listener");
        t1.start();
        Timer t = new Timer("Client Ping");
        TimerTask tt = new TimerTask() {
            public void run() {
            	synchronized(this) {
	                for(Socket s : availableHandlers) {
	                    try {
	                        SSocketHelper.writeToSocket(s, "PING:\n");
	                    }
	                    catch(IOException e) {
	                    	alg2.removeClient();
	                        switchPeer(peerPairs.get(s), s);
	                        peerPairs.remove(s);
	                        break;
	                    }
	                }
            	}
            }
        };
        t.schedule(tt, 0, 1000);
        listenForClients();
    }
    
    public void listenForClients()
    {
        while(true)
        {
            try
            {
                final Socket currentClient = clientListen.accept();
                System.out.println("Connection from: " + currentClient.getInetAddress().toString().substring(1, currentClient.getInetAddress().toString().length()));
                new Thread(new Runnable() {
                    public void run() {
                        try
                        {
                            Socket[] handlers = selectEventHandlers();
                            Socket selectedServer = handlers[0];
                            Socket selectedPeer = handlers[1];
                            if(selectedPeer == null) {
                                SSocketHelper.writeToSocket(currentClient, selectedServer.getInetAddress().toString().substring(1, selectedServer.getInetAddress().toString().length()) + "\n");
                            }
                            else {
                                SSocketHelper.writeToSocket(currentClient, selectedServer.getInetAddress().toString().substring(1, selectedServer.getInetAddress().toString().length()) + ":" + selectedPeer.getInetAddress().toString().substring(1, selectedPeer.getInetAddress().toString().length()) + "\n");
                                peerPairs.get(selectedPeer).add(currentClient);
                            }
                            peerPairs.put(currentClient, new ArrayList<Socket>());
                        }
                        catch(IOException e)
                        {
                        }
                        availableHandlers.add(currentClient);
                    }
                }).start();
            }
            catch(IOException e)
            {
            }
        }
    }
    
    public void listenForServers()
    {
        while(true)
        {
            try
            {
                final Socket server = serverListen.accept();
                System.out.println("Server connection from: " + server.getInetAddress().toString().substring(1, server.getInetAddress().toString().length()));
                availableSpecialHandlers.add(server);
                new Thread(new Runnable() {
                    public void run() {
                        while(true) {
                            try {
                                getStats(server);
                                String[] stats = serverStats.get(server).split(":");
                                int connectedClients = Integer.parseInt(stats[0]);
                                int maxEvents = Integer.parseInt(stats[1]);
                                int currentEvents = Integer.parseInt(stats[2]);
                                if(currentEvents >= maxEvents) {
                                    System.out.println();
                                    Socket toHandler = selectEventHandlers()[0];
                                    int numClientsToSend = (int) (connectedClients / 2);
                                    SSocketHelper.writeToSocket(server, "SEND:clients:" + toHandler.getInetAddress().toString().substring(1, toHandler.getInetAddress().toString().length()) + ":" + numClientsToSend + "\n");
                                }
                                
                                System.out.println(serverStats.get(server));
                            }
                            catch(Exception e) {
                                availableSpecialHandlers.remove(server);
                                e.printStackTrace();
                            }
                            try {
                                Thread.sleep(10000);
                            }
                            catch(InterruptedException e) {}
                        }
                    }
                }).start();

            }
            catch(IOException e)
            {
            }
        }
    }
    
    private synchronized void getStats(Socket s)
    {
        try {
            SSocketHelper.writeToSocket(s, "GET:Stats\n");
            String testMap = serverStats.get(s);
            if(testMap != null)
                serverStats.remove(s);
            serverStats.put(s,  SSocketHelper.readFromSocket(s, true));
        }
        catch(IOException e) {
            if(availableSpecialHandlers.contains(s))
                availableSpecialHandlers.remove(s);
            else
                availableHandlers.remove(s);
        }
    }
    
    private Socket[] selectEventHandlers()
    {
        Socket[] handlers = new Socket[2];
        long beforeTime = System.nanoTime();
        handlers[0] = alg.select(availableSpecialHandlers, false);
        long afterTime = System.nanoTime();
        System.out.println("Took " + (afterTime - beforeTime) + " nanoseconds to select server");
        beforeTime = System.nanoTime();
        try {
            handlers[1] = alg2.select(availableHandlers, true);
            afterTime = System.nanoTime();
            System.out.println("Took " + (afterTime - beforeTime) + " nanoseconds to select client");
        }
        catch(IndexOutOfBoundsException ex)
        {
            handlers[1] = null;
        }
        return handlers;
    }
    
    private void switchPeer(ArrayList<Socket> clients, Socket disconnected) {
        for(Socket client : clients) {
            long beforeTime = System.nanoTime();
            ArrayList<Socket> tempClientList = availableHandlers;
            tempClientList.remove(client);
            tempClientList.remove(disconnected);
            Socket newPeer = alg.select(tempClientList, true);
            long afterTime = System.nanoTime();
            System.out.println("Took " + (afterTime - beforeTime) + " nanoseconds to select client");
            try {
                SSocketHelper.writeToSocket(client, "CHNG:Peer:" + newPeer.getInetAddress().toString().substring(1, newPeer.getInetAddress().toString().length()) + "\n");
            }
            catch(IOException e)
            {
            }
            peerPairs.get(newPeer).add(client);
        }
    }


    public static void main(String[] args)
    {
        new LoadBalancerP2P(12000);
    }

}
