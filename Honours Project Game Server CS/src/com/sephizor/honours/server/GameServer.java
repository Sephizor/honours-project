package com.sephizor.honours.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import com.sephizor.honours.event.Event;
import com.sephizor.utils.net.SSocketHelper;

public class GameServer
{
    private ServerSocket clientListen;
    private ArrayList<Socket> clients;
    private Socket loadBalancer;
    private Timer timer;
    private TimerTask eventReset;
    private int eventLimit;
    private volatile int eventCounter;
    private volatile int eventsPerMin;

    public GameServer(String loadBalancerIP, int loadBalancerPort, int limit)
    {
        try
        {
            eventLimit = limit;
            eventCounter = 0;
            eventsPerMin = 0;
            clientListen = new ServerSocket(12010);
            clients = new ArrayList<Socket>();
            loadBalancer = new Socket(loadBalancerIP, loadBalancerPort);
        }
        catch(IOException e)
        {
            e.printStackTrace();
            System.exit(-1);
        }

        timer = new Timer("Event Timer");
        eventReset = new TimerTask() {
            public void run()
            {
                System.out.println("Resetting event counter");
                eventsPerMin = eventCounter * 4;
                eventCounter = 0;
            }
        };
        timer.schedule(eventReset, 0, 15 * 1000);

        Thread clientListener = new Thread(new Runnable() {
            public void run()
            {
                while(true)
                    listenForClients();
            }
        });
        clientListener.setName("Client Listener");
        clientListener.start();
        Thread loadBalancerListener = new Thread(new Runnable() {
            public void run()
            {
                while(true)
                    listenForRequests();
            }
        });
        loadBalancerListener.setName("Load Balancer Protocol");
        loadBalancerListener.start();
    }

    private void listenForClients()
    {
        try
        {
            final Socket client = clientListen.accept();
            clients.add(client);
            new Thread(new Runnable() {
                public void run()
                {
                    Socket thisClient = client;
                    while(true) {
                        try
                        {
                            ObjectInputStream sockIn = new ObjectInputStream(client.getInputStream());
                            //Blocking call to receive Events
                            Event event = (Event) sockIn.readObject();
                            processEvent(event);
                        }
                        catch(IOException | ClassNotFoundException e)
                        {
                            clients.remove(thisClient);
                            break;
                        }
                    }
                }

                private void processEvent(Event event)
                {
                    //Print the contents of the event
                    event.printPayload(client);
                    //Increment the event counter
                    eventCounter++;
                }
            }).start();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }

    }

    private void reportStats()
    {
        try
        {
            System.out.println("Reporting stats");
            SSocketHelper.writeToSocket(loadBalancer, clients.size() + ":" + eventLimit +  ":" + eventsPerMin + "\n");
        }
        catch(IOException e)
        {
            System.exit(-1);
        }
    }

    public void listenForRequests()
    {
        try {
            System.out.println("Listening for requests");
            String line = SSocketHelper.readFromSocket(loadBalancer, true);
            String[] request = line.split(":");
            String method = request[0];
            String function = request[1];
    
            if(method.equals("GET"))
            {
                switch(function)
                {
                    case "ReportStats":
                        reportStats();
                        break;
                    default:
                        break;
                }
            }
            else if(method.equals("SEND"))
            {
                switch(function)
                {
                    case "clients":
                        sendLoad(request[2], Integer.parseInt(request[3]));
                        break;
                    default:
                        break;
                }
            }
        }
        catch(IOException e) {
            System.exit(-1);
        }
    }

    private synchronized void sendLoad(String toIP, int numClients)
    {
        ArrayList<Socket> clientsToSend = new ArrayList<Socket>();
        int counter = 0;
        int listSize = clients.size()-1;
        for(int i = 0; i < numClients; i++)
        {
            //Ensure that we don't hit array indexes below 0
            if(listSize - counter >= 0)
            {
                //Add client from main list to secondary list
                clientsToSend.add(clients.get(listSize - counter));
                counter++;
            }
            else
            {
                //Else we're below 0
                break;
            }
        }
        for(Socket client : clientsToSend)
        {
            try
            {
                SSocketHelper.writeToSocket(client, "ChangeServer:" + toIP+"\n");
            }
            catch(IOException e)
            {
            }
            clients.remove(client);
        }
        eventsPerMin = 0;
    }

    public static void main(String[] args)
    {
        int numEvents = 0;
        int lbPort = 0;
        try
        {
            lbPort = Integer.parseInt(args[1]);
            numEvents = Integer.parseInt(args[2]);
        }
        catch(NumberFormatException e)
        {
            lbPort = 12000;
            numEvents = 100;
        }
        new GameServer(args[0], lbPort, numEvents);
    }
}
