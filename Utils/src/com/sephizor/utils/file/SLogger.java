package com.sephizor.utils.file;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class provides the ability to log any program output to a file of your choice
 * @author Stephen Bayne
 */
public class SLogger {
    private File logFile;
    private static SLogger logger;
    
    private SLogger(String filePath, String fileName)
    {
        File logDir = new File(filePath);
        if(!logDir.exists());
            logDir.mkdirs();
        logFile = new File(filePath + File.separator + fileName);
        if(!logFile.exists())
        {
            try {
                logFile.createNewFile();
            }
            catch(IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * Get a static, singleton instance of the logger class
     * @param filePath
     * @param fileName
     * @return static instance of the logger
     */
    public static SLogger getInstance(String filePath, String fileName)
    {
        if(logger == null)
        {
            logger = new SLogger(filePath, fileName);
        }
        return logger;
    }
    
    /**
     * Writes out a debug message to the log file
     * @param message
     */
    public void debugMessage(String message) {
        StringBuilder sb = new StringBuilder();
        sb.append(getDate());
        sb.append(": DEBUG: ");
        sb.append(message);
        sb.append("\n");
        
        writeFile(sb.toString());
    }
    
    /**
     * Writes out an information message to the log file
     * @param message
     */
    public void infoMessage(String message) {
        StringBuilder sb = new StringBuilder();
        sb.append(getDate());
        sb.append(": INFO: ");
        sb.append(message);
        sb.append("\n");
        
        writeFile(sb.toString());
    }
    
    /**
     * Writes out a warning message to the log file
     * @param message
     */
    public void warningMessage(String message) {
        StringBuilder sb = new StringBuilder();
        sb.append(getDate());
        sb.append(": WARN: ");
        sb.append(message);
        sb.append("\n");
        
        writeFile(sb.toString());
    }
    
    /**
     * Writes out an error message to the log file
     * @param message
     */
    public void errorMessage(String message) {
        StringBuilder sb = new StringBuilder();
        sb.append(getDate());
        sb.append(": ERROR: ");
        sb.append(message);
        sb.append("\n");
        
        writeFile(sb.toString());
    }
    
    /**
     * Writes an <code>Exception</code>'s output to the log file
     * @param thrownException
     */
    public void errorMessage(Throwable thrownException)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(getDate());
        sb.append(": ERROR: ");
        for(StackTraceElement e : thrownException.getStackTrace())
        {
            sb.append(e.toString());
            sb.append("\n");
        }
        writeFile(sb.toString());
    }
    
    /**
     * Performs a write to the file
     * @param message
     */
    private void writeFile(String message)
    {
        BufferedWriter writer;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(logFile, true)));
            writer.write(message);
            writer.flush();
        }
        catch(FileNotFoundException e) {
            e.printStackTrace();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }
    
    public String getDate()
    {
        return new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());
    }
}
