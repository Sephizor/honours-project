package com.sephizor.utils.net;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class SSocketHelper
{
    public static void writeToSocket(Socket s, String data) throws IOException
    {
        BufferedWriter sockOut = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
        sockOut.write(data);
        sockOut.flush();
    }
    
    public static String readFromSocket(Socket s, boolean singleLine) throws IOException
    {
        BufferedReader sockIn = new BufferedReader(new InputStreamReader(s.getInputStream()));
        String line = "";
        StringBuilder sb = new StringBuilder();
        if(!singleLine) {
            while((line = sockIn.readLine()) != null)
            {
                sb.append(line);
            }
            sockIn.close();
        }
        else {
            sb.append(sockIn.readLine());
        }
        return sb.toString();
    }
}
